﻿using LibraryManagement.DataAccess.UnitOfWork;
using LibraryManagement.DataAccess;
using LibraryManagement.DTO.BookDTO;
using Microsoft.AspNetCore.Mvc;
using LibraryManagement.DTO.AuthorDTO;
using LibraryManagement.Model;

namespace LibraryManagement.Controller
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class AuthorController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly AppDbContext _context;
        private static List<AuthorDTO> _authorsDTO = new List<AuthorDTO>();
        public AuthorController(AppDbContext context)
        {
            _context = context;
            unitOfWork = new UnitOfWork(_context);
        }

        [HttpGet]
        public List<AuthorDTO> GetAllAuthor()
        {
            var authors = unitOfWork.AuthorRepository.GetAll();
            foreach (var author in authors)
            {
                AuthorDTO authorDTO = new AuthorDTO();
                authorDTO.ID = author.Id;
                authorDTO.Name = author.Name;
                _authorsDTO.Add(authorDTO);
            }
            return _authorsDTO;
        }

        [HttpGet("{id}")]
        public AuthorDTO GetAuthorById(int id)
        {
            var author = unitOfWork.AuthorRepository.GetByID(id);
            AuthorDTO authorDTO = new AuthorDTO();
            authorDTO.ID = author.Id;
            authorDTO.Name = author.Name;
            return authorDTO;
        }

        [HttpPost]
        public void CreateAuthor([FromBody] AuthorDTO authorDTO)
        {
            Author author = new Author();
            author.Name = authorDTO.Name;
            unitOfWork.AuthorRepository.Add(author);
            unitOfWork.SaveChange();
        }

        [HttpPut("{id}")]
        public void UpdateAuthor(int id, [FromBody] AuthorDTO authorDTO)
        {
            var author = unitOfWork.AuthorRepository.GetByID(id);
            author.Name = authorDTO.Name;
            unitOfWork.AuthorRepository.Update(author);
            unitOfWork.SaveChange();
        }

        [HttpDelete("{id}")]
        public void DeleteAuthor(int id)
        {
            var author = unitOfWork.AuthorRepository.GetByID(id);
            unitOfWork.AuthorRepository.Delete(author);
            unitOfWork.SaveChange();
        }
    }
}
