﻿using LibraryManagement.DataAccess;
using LibraryManagement.DataAccess.UnitOfWork;
using LibraryManagement.DTO.BookDTO;
using LibraryManagement.Model;
using Microsoft.AspNetCore.Mvc;

namespace LibraryManagement.Controller
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class BookController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly AppDbContext _context;
        private static List<BookDTO> _booksDTO = new List<BookDTO>();
        public BookController(AppDbContext context)
        {
            _context = context;
            unitOfWork = new UnitOfWork(_context);
        }

        [HttpGet]
        public List<BookDTO> GetAllBook()
        {
            var books = unitOfWork.BookRepository.GetAll();
            foreach (var book in books)
            {
                _context.Entry(book).Reference(book => book.Author).Load();
                BookDTO bookDTO = new BookDTO();
                bookDTO.Id = book.Id;
                bookDTO.AuthorID = book.AuthorId;
                bookDTO.Title = book.Title;
                bookDTO.AuthorName = book.Author.Name;
                bookDTO.ISBN = book.ISBN;
                book.PublishDate = book.PublishDate;
                _booksDTO.Add(bookDTO);
            }
            return _booksDTO;
        }
        [HttpGet("{id}")]
        public BookDTO GetBookByID(int id)
        {
            BookDTO showBook = new BookDTO();
            var book = unitOfWork.BookRepository.GetByID(id);
            _context.Entry(book).Reference(book => book.Author).Load();
            showBook.Id = book.Id;
            showBook.AuthorID = book.AuthorId;
            showBook.Title = book.Title;
            showBook.AuthorName = book.Author.Name;
            showBook.ISBN = book.ISBN;
            showBook.PublishDate = book.PublishDate;
            return showBook;
        }
        [HttpPost]
        public void CreateBook([FromBody] BookCreateDTO bookAdd)
        {
            Book book = new Book();
            book.AuthorId = bookAdd.AuthorId;
            book.Title = bookAdd.Title;
            book.ISBN = bookAdd.ISBN;
            book.PublishDate = bookAdd.PublishDate;
            unitOfWork.BookRepository.Add(book);
            unitOfWork.SaveChange();
        }
        [HttpPut("{id}")]
        public void UpdateBook(int id, [FromBody] BookUpdateDTO bookUpdate)
        {
            var book = unitOfWork.BookRepository.GetByID(id);
            book.AuthorId = bookUpdate.AuthorId;
            book.Title= bookUpdate.Title;
            book.ISBN = bookUpdate.ISBN;
            book.PublishDate = bookUpdate.PublishDate;
            unitOfWork.BookRepository.Update(book);
            unitOfWork.SaveChange();
        }
        [HttpDelete("{id}")]
        public void DeleteBook(int id)
        {
            var book = unitOfWork.BookRepository.GetByID(id);
            unitOfWork.BookRepository.Delete(book);
            unitOfWork.SaveChange();
        }
    }
}
