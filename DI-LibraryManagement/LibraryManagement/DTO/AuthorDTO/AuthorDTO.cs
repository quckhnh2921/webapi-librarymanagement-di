﻿namespace LibraryManagement.DTO.AuthorDTO
{
    public class AuthorDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
