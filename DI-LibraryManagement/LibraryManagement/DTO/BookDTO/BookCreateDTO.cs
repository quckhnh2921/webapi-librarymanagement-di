﻿namespace LibraryManagement.DTO.BookDTO
{
    public class BookCreateDTO
    {
        public int AuthorId { get; set; }
        public string ISBN { get; set; }
        public string Title { get; set; }
        public DateTime PublishDate { get; set; }
    }
}
