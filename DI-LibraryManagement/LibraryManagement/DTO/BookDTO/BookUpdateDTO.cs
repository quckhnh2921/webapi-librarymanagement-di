﻿namespace LibraryManagement.DTO.BookDTO
{
    public class BookUpdateDTO
    {
        public int AuthorId { get; set; }
        public string ISBN { get; set; }
        public DateTime PublishDate { get; set; }
        public string Title { get; set; }
    }
}
