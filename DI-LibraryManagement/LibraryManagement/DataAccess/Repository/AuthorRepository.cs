﻿using LibraryManagement.DataAccess.IRepository;
using LibraryManagement.Model;

namespace LibraryManagement.DataAccess.Repository
{
    public class AuthorRepository : GenericRepository<Author>, IAuthorRepository
    {
        public AuthorRepository(AppDbContext context) : base(context)
        {
        }
    }
}
