﻿using LibraryManagement.DataAccess.IRepository;
using LibraryManagement.DataAccess.Repository;

namespace LibraryManagement.DataAccess.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        private readonly IAuthorRepository _authorRepository;
        private readonly IBookRepository _bookRepository;
        public UnitOfWork(AppDbContext context)
        {
            _context = context;
            _authorRepository = new AuthorRepository(context);
            _bookRepository = new BookRepository(context);
        }
        public IBookRepository BookRepository { get => _bookRepository; }

        public IAuthorRepository AuthorRepository { get => _authorRepository; }

        public int SaveChange()
        {
            return _context.SaveChanges();
        }
    }
}
