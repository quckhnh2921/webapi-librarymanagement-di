using LibraryManagement.DataAccess;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
var sqlConnection = builder.Configuration.GetValue<string>("ConnectionStrings:ConnectionSQL");

builder.Services.AddDbContext<AppDbContext>(options => options.UseSqlServer(sqlConnection));
builder.Services.AddControllers();

var app = builder.Build();
app.MapControllers();
app.Run();

