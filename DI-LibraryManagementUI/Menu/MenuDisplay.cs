﻿namespace DI_LibraryManagementUI.Menu
{
    public static class MenuDisplay
    {
        public static void MainMenu()
        {
            Console.WriteLine("=============MAIN MENU=============");
            Console.WriteLine("| 1. Author menu                  |");
            Console.WriteLine("| 2. Book menu                    |");
            Console.WriteLine("| 3. Exit                         |");
            Console.WriteLine("====================================");
            Console.Write("Choice: ");
        }

        public static void AuthorMenu()
        {
            Console.WriteLine("==============AUTHOR MENU==============");
            Console.WriteLine("| 1. Show all author                  |");
            Console.WriteLine("| 2. Get author by id                 |");
            Console.WriteLine("| 3. Create author                    |");
            Console.WriteLine("| 4. Update author                    |");
            Console.WriteLine("| 5. Delete author                    |");
            Console.WriteLine("| 6. Exit                             |");
            Console.WriteLine("=======================================");
            Console.Write("Choice: ");
        }

        public static void BookMenu()
        {
            Console.WriteLine("==============BOOK MENU================");
            Console.WriteLine("| 1. Show all Book                    |");
            Console.WriteLine("| 2. Get book by id                   |");
            Console.WriteLine("| 3. Create book                      |");
            Console.WriteLine("| 4. Update book                      |");
            Console.WriteLine("| 5. Delete book                      |");
            Console.WriteLine("| 6. Exit                             |");
            Console.WriteLine("=======================================");
            Console.Write("Choice: ");
        }
    }
}
