﻿namespace DI_LibraryManagementUI.Model
{
    public class Author
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
