﻿namespace DI_LibraryManagementUI.Model
{
    public class Book
    {
        public int Id { get; set; }
        public string AuthorName { get; set; }
        public string Title { get; set; }
        public int AuthorID { get; set; }
        public string ISBN { get; set; }
        public DateTime PublishDate { get; set; }
    }
}

