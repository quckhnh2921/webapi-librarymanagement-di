﻿using DI_LibraryManagementUI.Menu;
using DI_LibraryManagementUI.Service.AuthorService;
using DI_LibraryManagementUI.Service.BookService;

AuthorService authorService = new AuthorService();
BookService bookService = new BookService();
int choice = 0;
do
{
    try
    {
        MenuDisplay.MainMenu();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                int authorChoice = 0;
                do
                {
                    await authorService.GetAllAuthor();
                    try
                    {
                        MenuDisplay.AuthorMenu();
                        authorChoice = int.Parse(Console.ReadLine());
                        switch (authorChoice)
                        {
                            case 1:
                                await authorService.ShowAllAuthor();
                                break;
                            case 2:
                                var author = await authorService.GetAuthorById();
                                Console.WriteLine($"{author.ID} - {author.Name}");
                                break;
                            case 3:
                                await authorService.CreateAuthor();
                                break;
                            case 4:
                                await authorService.UpdateAuthor();
                                break;
                            case 5:
                                await authorService.DeleteAuthor();
                                break;
                            case 6:
                                break;
                            default:
                                throw new Exception("Please choose from 1 to 6");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                } while (authorChoice != 6);
                break;
            case 2:
                int bookChoice = 0;
                do
                {
                    await bookService.GetAllBook();
                    try
                    {
                        MenuDisplay.BookMenu();
                        bookChoice = int.Parse(Console.ReadLine());
                        switch (bookChoice)
                        {
                            case 1:
                                await bookService.ShowAllBook();
                                break;
                            case 2:
                                await bookService.GetBookById();
                                break;
                            case 3:
                                await bookService.CreateBook();
                                break;
                            case 4:
                                await bookService.UpdateBook();
                                break;
                            case 5:
                                await bookService.DeleteBook();
                                break;
                            case 6:
                                break;
                            default:
                                throw new Exception("Please choose from 1 to 6");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                } while (bookChoice != 6);
                break;
            case 3:
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Please choose from 1 to 3");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 3);