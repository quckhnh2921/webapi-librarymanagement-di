﻿using DI_LibraryManagementUI.Model;
using Newtonsoft.Json;

namespace DI_LibraryManagementUI.Service.AuthorService
{
    public class AuthorService
    {
        List<Author> authors = new List<Author>();
        private const string GET_ALL_AUTHOR = $"http://localhost:5000/author/getallauthor";
        private const string GET_AUTHOR_BY_ID = $"http://localhost:5000/author/getauthorbyid/";
        private const string CREATE_AUTHOR = $"http://localhost:5000/author/createauthor/";
        private const string UPDATE_AUTHOR = $"http://localhost:5000/author/updateauthor/";
        private const string DELETE_AUTHOR = $"http://localhost:5000/author/deleteauthor/";
        private HttpClient httpClient;
        public AuthorService()
        {
            httpClient = new HttpClient();
        }
        public async Task GetAllAuthor()
        {
            var request = new HttpRequestMessage(HttpMethod.Get, GET_ALL_AUTHOR);
            var response = await httpClient.SendAsync(request);
            string content = await response.Content.ReadAsStringAsync();
            authors = JsonConvert.DeserializeObject<List<Author>>(content);
        }

        public async Task ShowAllAuthor()
        {
            foreach (var author in authors)
            {
                await Console.Out.WriteLineAsync($"{author.ID} - {author.Name}");
            }
        }

        public async Task<Author> GetAuthorById()
        {
            await Console.Out.WriteLineAsync("Enter author id: ");
            int id = int.Parse(Console.ReadLine());
            var request = new HttpRequestMessage(HttpMethod.Get, GET_AUTHOR_BY_ID +  id);
            var response = await httpClient.SendAsync(request);
            string content = await response.Content.ReadAsStringAsync();
            Author author = new Author();
            author = JsonConvert.DeserializeObject<Author>(content);
            return author;
        }

        public async Task CreateAuthor()
        {
            var request = new HttpRequestMessage(HttpMethod.Post, CREATE_AUTHOR);
            await Console.Out.WriteLineAsync("Enter author name: ");
            string name = Console.ReadLine();
            var content = new StringContent($"{{\"Name\":\"{name}\"}}", System.Text.Encoding.UTF8, "application/json");
            request.Content = content;
            await httpClient.SendAsync(request);
        }
        public async Task UpdateAuthor()
        {
            await Console.Out.WriteLineAsync("Enter author id to update: ");
            int id = int.Parse(Console.ReadLine());
            var request = new HttpRequestMessage(HttpMethod.Put, UPDATE_AUTHOR + id);
            await Console.Out.WriteLineAsync("Enter new author name: ");
            string name = Console.ReadLine();
            var content = new StringContent($"{{\"Name\": \"{name}\"}}", System.Text.Encoding.UTF8, "application/json");
            request.Content = content;
            await httpClient.SendAsync(request);
        }
        public async Task DeleteAuthor()
        {
            await Console.Out.WriteLineAsync("Enter author id to update: ");
            int id = int.Parse(Console.ReadLine());
            var request = new HttpRequestMessage(HttpMethod.Delete, DELETE_AUTHOR + id);
            await httpClient.SendAsync(request);
        }
    }
}
