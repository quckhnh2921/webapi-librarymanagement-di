﻿using DI_LibraryManagementUI.Model;
using Newtonsoft.Json;

namespace DI_LibraryManagementUI.Service.BookService
{
    public class BookService
    {
        private const string GET_ALL_BOOK = $"http://localhost:5000/book/getallbook";
        private const string GET_BOOK_BY_ID = $"http://localhost:5000/book/getbookbyid/";
        private const string CREATE_BOOK = $"http://localhost:5000/book/createbook/";
        private const string UPDATE_BOOK = $"http://localhost:5000/book/updatebook/";
        private const string DELETE_BOOK = $"http://localhost:5000/book/deletebook/";
        private List<Book> books;

        private HttpClient httpClient;
        public BookService()
        {
            httpClient = new HttpClient();
            books = new List<Book>();
        }

        public async Task<List<Book>> GetAllBook()
        {
            var request = new HttpRequestMessage(HttpMethod.Get, GET_ALL_BOOK);
            var response = await httpClient.SendAsync(request);
            var content = await response.Content.ReadAsStringAsync();
            books = JsonConvert.DeserializeObject<List<Book>>(content);
            return books;
        }

        public async Task ShowAllBook()
        {
            foreach (var book in books)
            {
                await Console.Out.WriteLineAsync($"{book.Id} - {book.AuthorID} - {book.AuthorName} - {book.Title} - {book.ISBN} - {book.PublishDate.ToString("dd/MM/yyyy")}");
            }
        }

        public async Task<Book> GetBookById()
        {
            await Console.Out.WriteLineAsync("Enter book id: ");
            int id = int.Parse(Console.ReadLine());
            var request = new HttpRequestMessage(HttpMethod.Get, GET_BOOK_BY_ID + id);
            var response = await httpClient.SendAsync(request);
            string content = await response.Content.ReadAsStringAsync();
            Book book = new Book();
            book = JsonConvert.DeserializeObject<Book>(content);
            return book;
        }

        public async Task CreateBook()
        {
            var request = new HttpRequestMessage(HttpMethod.Post, CREATE_BOOK);
            await Console.Out.WriteLineAsync("Enter author id: ");
            int authorID = int.Parse(Console.ReadLine());
            await Console.Out.WriteLineAsync("Enter book title: ");
            string bookTitle = Console.ReadLine();
            await Console.Out.WriteLineAsync("Enter book ISBN: ");
            string ISBN = Console.ReadLine();
            await Console.Out.WriteLineAsync("Enter publish date: ");
            string date = Console.ReadLine();

            var content = new StringContent($"{{\"AuthorId\": {authorID}, " +
                                            $"\"Title\":\"{bookTitle}\"," +
                                            $" \"ISBN\": \"{ISBN}\"," +
                                            $"\"PublishDate\": \"{date}\"}}"
                                            , System.Text.Encoding.UTF8, "application/json");
            request.Content = content;
            await httpClient.SendAsync(request);
        }

        public async Task UpdateBook()
        {
            await Console.Out.WriteLineAsync("Enter book id: ");
            int id = int.Parse(Console.ReadLine());
            var request = new HttpRequestMessage(HttpMethod.Put, UPDATE_BOOK + id);
            await Console.Out.WriteLineAsync("Enter author id: ");
            int authorID = int.Parse(Console.ReadLine());
            await Console.Out.WriteLineAsync("Enter book title: ");
            string bookTitle = Console.ReadLine();
            await Console.Out.WriteLineAsync("Enter book ISBN: ");
            string ISBN = Console.ReadLine();
            await Console.Out.WriteLineAsync("Enter publish date: ");
            string date = Console.ReadLine();

            var content = new StringContent($"{{\"AuthorId\": {authorID}, " +
                                            $"\"Title\":\"{bookTitle}\"," +
                                            $" \"ISBN\": \"{ISBN}\"," +
                                            $"\"PublishDate\": \"{date}\"}}"
                                            , System.Text.Encoding.UTF8, "application/json");

            request.Content = content;
            await httpClient.SendAsync(request);
        }

        public async Task DeleteBook()
        {
            await Console.Out.WriteLineAsync("Enter book id to delete: ");
            int id = int.Parse(Console.ReadLine());
            var request = new HttpRequestMessage(HttpMethod.Delete, DELETE_BOOK + id);
            await httpClient.SendAsync(request);
        }
    }
}
